﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Proshot.CommandClient;
using System.Threading;
using OnBarcode.Barcode.BarcodeScanner;
using BarcodeLib.BarcodeReader;

namespace markitt_toten
{

    public partial class Toten : Form
    {
        // Client Queue Form
        private ClientQueue queueForm;
        String clientSelected = null;
        String incomingMessage;
        List<string> clientList;       

        // Video and Scan Code
        AForge.Video.DirectShow.VideoCaptureDevice videoSource;
        string fileTitle1 = "snapshot.png";
        protected String[] barcodes;
        protected String barcodeRead;

        // Communication
        private Proshot.CommandClient.CMDClient client;
        IPAddress ipServer = IPAddress.Parse("127.0.0.1");

        public Toten()
        {
            InitializeComponent();
            StartLogin();
            InitializeCameras();
            StartScanning();
        }

        private void StartLogin()
        {
            this.client = new CMDClient(ipServer, 8000, "Toten");
            this.client.CommandReceived += new CommandReceivedEventHandler(client_CommandReceived);

            // Start timerLogion
            timerLogin.Enabled = true;
            timerLogin.Interval = 5000;
            timerLogin.Tick += new System.EventHandler(this.timerLogin_Tick);
        }

        void client_CommandReceived(object sender, CommandEventArgs e)
        {
            switch (e.Command.CommandType)
            {
                case (Proshot.CommandClient.CommandType.TotenClientQueueRequest):
                    incomingMessage = e.Command.MetaData;
                    clientList = incomingMessage.Split(',').ToList();
                    clientList.Remove(clientList.Last());

                    if (clientSelected == null)
                        queueForm = new ClientQueue(clientList, true, this);
                    else
                        queueForm = new ClientQueue(clientList, false, this);

                    this.Hide();
                    queueForm.ShowDialog(); 
                    break;
            }
        }

        private void InitializeCameras()
        {
            AForge.Video.DirectShow.FilterInfoCollection videosources = new AForge.Video.DirectShow.FilterInfoCollection(AForge.Video.DirectShow.FilterCategory.VideoInputDevice);

            if (videosources != null)
            {
                // Para alterar qual câmera usar, basta alterar o índice i de videosources[i].MonikerString
                videoSource = new AForge.Video.DirectShow.VideoCaptureDevice(videosources[4].MonikerString);
                videoSource.NewFrame += (s, e) => webCamPictureBox.Image = (Bitmap)e.Frame.Clone();
                videoSource.Start();
                Thread.Sleep(7000);
            }
        }

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            if (this.client.Connected == false)
            {
                textBoxMessage.Text = "Conectando com servidor...";
                buttonDispatch.Enabled = false;
                buttonQueue.Enabled = false;
                this.client.ConnectToServer();
            }
            else
            {
                textBoxMessage.Text = "Selecione um cliente da lista";                
                buttonQueue.Enabled = true;
                this.timerLogin.Enabled = false;
            }
        }

        private void StartScanning()
        {
            // Start timer1
            timerCam.Enabled = false;
            timerCam.Interval = 500;
            timerCam.Tick += new EventHandler(this.timerCam_Tick);
            Thread.Sleep(250);          
         }

        private void timerCam_Tick(object sender, EventArgs e)
        {
            if (videoSource.IsRunning && webCamPictureBox.Image != null)
            {
                webCamPictureBox.Image.Save(fileTitle1, System.Drawing.Imaging.ImageFormat.Png);

                Bitmap imageFrame = (Bitmap)Image.FromFile(fileTitle1, true);

                //barcodes = BarcodeScanner.ScanSingleBarcode(imageFrame, BarcodeType.QRCode);
                barcodes = BarcodeReader.read(imageFrame, BarcodeReader.QRCODE);

                if (barcodes != null && this.client.Connected && clientSelected != null)
                {
                    buttonRegister.Visible = true;
                    buttonManualRead.Enabled = false;
                    buttonQueue.Enabled = false;

                    // remove primeiro caractere da string
                    string barcodeRead = barcodes.First();
                    barcodeRead = barcodeRead.Substring(1, barcodeRead.Length - 1);
                }

                imageFrame.Dispose();
            }
        }

        private void buttonRegisterClick(object sender, EventArgs e)
        {
            // enviar código para o servidor
            if (this.client.Connected)
            {
                if (textBoxManualRead.Visible == false) // Auto Read
                {
                    string message = barcodeRead + ',' + clientSelected;
                    this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.QRCodeToten, ipServer, message));
                    buttonRegister.Visible = false;
                    buttonManualRead.Enabled = true;
                    buttonQueue.Enabled = true;
                    MessageBox.Show("Bag Cadastrada!");
                }
                else if (!String.IsNullOrWhiteSpace(textBoxManualRead.Text)) // Manual Read
                {
                    string message = textBoxManualRead.Text + ',' + clientSelected;
                    this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.QRCodeToten, ipServer, message));
                    textBoxManualRead.Clear();
                    MessageBox.Show("Bag Cadastrada!");
                }
            }
            else
            {
                this.timerLogin.Enabled = true;
            }            
        }

        private void buttonQueue_Click(object sender, EventArgs e)
        {
            // enviar requisição de lista de clientes para o servidor
            if (this.client.Connected)
            {
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.TotenClientQueueRequest, ipServer, null));
            }
            else
            {
                this.timerLogin.Enabled = true;
            }                      
        }

        public void displaySeletedClient(string p)
        {
            buttonManualRead.Enabled = true;
            timerCam.Enabled = true;
            clientSelected = p;
            textBoxMessage.Text = p;
            buttonDispatch.Enabled = true;
            buttonRegister.Enabled = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            this.client.Disconnect();

            if (videoSource != null && videoSource.IsRunning)
            {
                videoSource.SignalToStop();
                videoSource = null;
            }
        }

        private void buttonManualRead_Click(object sender, EventArgs e)
        {
            if (this.client.Connected)
            {
                webCamPictureBox.Visible = false;
                textBoxManualRead.Visible = true;
                labelManualRead.Visible = true;
                buttonManualRead.Visible = false;
                buttonAutoRead.Visible = true;
                buttonRegister.Visible = true;
                timerCam.Enabled = false;                
            }
        }

        private void buttonAutoRead_Click(object sender, EventArgs e)
        {
            webCamPictureBox.Visible = true;
            textBoxManualRead.Visible = false;
            labelManualRead.Visible = false;
            buttonManualRead.Visible = true;
            buttonAutoRead.Visible = false;
            buttonRegister.Visible = false;
            timerCam.Enabled = true;
        }

        private void buttonDispatch_Click(object sender, EventArgs e)
        {
            // enviar informação de encerramento de descarga de cliente para o servidor
            if (this.client.Connected && !String.IsNullOrEmpty(clientSelected))
            {
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.FinishClientDispatch, ipServer, clientSelected));
                MessageBox.Show("Descarga do cliente " + clientSelected + " finalizada!");                
                clientSelected = null;
                textBoxMessage.Text = "Selecione um cliente da lista";
                buttonDispatch.Enabled = false;
                buttonRegister.Enabled = false;
                Thread.Sleep(1000);
                buttonQueue.PerformClick();
            }
            else
            {
                this.timerLogin.Enabled = true;
            }
        }
    }
}
