﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace markitt_toten
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>      
        public static Toten TotenForm;
        [STAThread]        
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TotenForm = new Toten();
            Application.Run(TotenForm);
        }
    }
}
