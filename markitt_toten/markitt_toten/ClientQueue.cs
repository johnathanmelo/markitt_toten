﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace markitt_toten
{
    public partial class ClientQueue : Form
    {
        Form totenForm;
        List<string> clientList;
        string clientSelected;
        bool canSelect;

        public ClientQueue(List<string> _clientList, bool _canSelect, Form _totenForm)
        {
            totenForm = _totenForm;
            clientList = _clientList;
            canSelect = _canSelect;

            InitializeComponent();
            FillCheckListBox();
        }

        private void FillCheckListBox()
        {
            // decide se tela será apenas para visualização da lista ou para seleção
            if (canSelect == true)
                buttonSelect.Enabled = true;
            else
                buttonSelect.Enabled = false;

            foreach (string p in clientList)
            {
                checkedListBox.Items.Add(p);
            }
        }        

        private void checkedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {            
            if (e.NewValue == CheckState.Checked)
            {               
                for (int ix = 0; ix < checkedListBox.Items.Count; ++ix)
                {
                    if (e.Index != ix) checkedListBox.SetItemChecked(ix, false);
                }
            }       
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            for (int ix = 0; ix < checkedListBox.Items.Count; ++ix)
            {
                if (checkedListBox.GetItemChecked(ix) == true)
                {
                    clientSelected = clientList[ix];
                    Program.TotenForm.displaySeletedClient(clientSelected);
                    this.Close();
                    totenForm.Show();
                }
            }            
        }        

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        protected override void OnClosed(EventArgs e)
        {
            totenForm.Show();
            base.OnClosed(e);
        }
    }
}
