﻿namespace markitt_toten
{
    partial class Toten
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerLogin = new System.Windows.Forms.Timer(this.components);
            this.buttonRegister = new System.Windows.Forms.Button();
            this.buttonDispatch = new System.Windows.Forms.Button();
            this.timerCam = new System.Windows.Forms.Timer(this.components);
            this.labelOpenQueue = new System.Windows.Forms.Label();
            this.textBoxManualRead = new System.Windows.Forms.TextBox();
            this.labelManualRead = new System.Windows.Forms.Label();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.buttonAutoRead = new System.Windows.Forms.Button();
            this.buttonManualRead = new System.Windows.Forms.Button();
            this.buttonQueue = new System.Windows.Forms.Button();
            this.webCamPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.webCamPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonRegister
            // 
            this.buttonRegister.BackColor = System.Drawing.Color.Green;
            this.buttonRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegister.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonRegister.Location = new System.Drawing.Point(520, 617);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(366, 60);
            this.buttonRegister.TabIndex = 2;
            this.buttonRegister.Text = "CADASTRAR BAG";
            this.buttonRegister.UseVisualStyleBackColor = false;
            this.buttonRegister.Visible = false;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegisterClick);
            // 
            // buttonDispatch
            // 
            this.buttonDispatch.BackColor = System.Drawing.Color.Black;
            this.buttonDispatch.Enabled = false;
            this.buttonDispatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDispatch.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonDispatch.Location = new System.Drawing.Point(1110, 610);
            this.buttonDispatch.Name = "buttonDispatch";
            this.buttonDispatch.Size = new System.Drawing.Size(228, 79);
            this.buttonDispatch.TabIndex = 3;
            this.buttonDispatch.Text = "FINALIZAR DESCARGA";
            this.buttonDispatch.UseVisualStyleBackColor = false;
            this.buttonDispatch.Click += new System.EventHandler(this.buttonDispatch_Click);
            // 
            // labelOpenQueue
            // 
            this.labelOpenQueue.AutoSize = true;
            this.labelOpenQueue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOpenQueue.Location = new System.Drawing.Point(1256, 102);
            this.labelOpenQueue.Name = "labelOpenQueue";
            this.labelOpenQueue.Size = new System.Drawing.Size(92, 20);
            this.labelOpenQueue.TabIndex = 5;
            this.labelOpenQueue.Text = "VER LISTA";
            // 
            // textBoxManualRead
            // 
            this.textBoxManualRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxManualRead.Location = new System.Drawing.Point(566, 376);
            this.textBoxManualRead.Name = "textBoxManualRead";
            this.textBoxManualRead.Size = new System.Drawing.Size(271, 41);
            this.textBoxManualRead.TabIndex = 7;
            this.textBoxManualRead.Visible = false;
            // 
            // labelManualRead
            // 
            this.labelManualRead.AutoSize = true;
            this.labelManualRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelManualRead.Location = new System.Drawing.Point(574, 337);
            this.labelManualRead.Name = "labelManualRead";
            this.labelManualRead.Size = new System.Drawing.Size(254, 36);
            this.labelManualRead.TabIndex = 9;
            this.labelManualRead.Text = "CÓDIGO DA BAG";
            this.labelManualRead.Visible = false;
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMessage.Location = new System.Drawing.Point(421, 41);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.Size = new System.Drawing.Size(559, 38);
            this.textBoxMessage.TabIndex = 10;
            this.textBoxMessage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonAutoRead
            // 
            this.buttonAutoRead.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonAutoRead.BackgroundImage = global::markitt_toten.Properties.Resources.cam_icon;
            this.buttonAutoRead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAutoRead.Location = new System.Drawing.Point(12, 24);
            this.buttonAutoRead.Name = "buttonAutoRead";
            this.buttonAutoRead.Size = new System.Drawing.Size(75, 75);
            this.buttonAutoRead.TabIndex = 8;
            this.buttonAutoRead.UseVisualStyleBackColor = false;
            this.buttonAutoRead.Visible = false;
            this.buttonAutoRead.Click += new System.EventHandler(this.buttonAutoRead_Click);
            // 
            // buttonManualRead
            // 
            this.buttonManualRead.BackgroundImage = global::markitt_toten.Properties.Resources.warning_icon_hi;
            this.buttonManualRead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonManualRead.Enabled = false;
            this.buttonManualRead.Location = new System.Drawing.Point(12, 24);
            this.buttonManualRead.Name = "buttonManualRead";
            this.buttonManualRead.Size = new System.Drawing.Size(75, 75);
            this.buttonManualRead.TabIndex = 6;
            this.buttonManualRead.UseVisualStyleBackColor = true;
            this.buttonManualRead.Click += new System.EventHandler(this.buttonManualRead_Click);
            // 
            // buttonQueue
            // 
            this.buttonQueue.BackgroundImage = global::markitt_toten.Properties.Resources.icon_list;
            this.buttonQueue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonQueue.Enabled = false;
            this.buttonQueue.Location = new System.Drawing.Point(1263, 24);
            this.buttonQueue.Name = "buttonQueue";
            this.buttonQueue.Size = new System.Drawing.Size(75, 75);
            this.buttonQueue.TabIndex = 4;
            this.buttonQueue.UseVisualStyleBackColor = true;
            this.buttonQueue.Click += new System.EventHandler(this.buttonQueue_Click);
            // 
            // webCamPictureBox
            // 
            this.webCamPictureBox.Location = new System.Drawing.Point(266, 93);
            this.webCamPictureBox.Name = "webCamPictureBox";
            this.webCamPictureBox.Size = new System.Drawing.Size(866, 439);
            this.webCamPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.webCamPictureBox.TabIndex = 0;
            this.webCamPictureBox.TabStop = false;
            // 
            // Toten
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 726);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.labelManualRead);
            this.Controls.Add(this.buttonAutoRead);
            this.Controls.Add(this.textBoxManualRead);
            this.Controls.Add(this.buttonManualRead);
            this.Controls.Add(this.labelOpenQueue);
            this.Controls.Add(this.buttonQueue);
            this.Controls.Add(this.buttonDispatch);
            this.Controls.Add(this.buttonRegister);
            this.Controls.Add(this.webCamPictureBox);
            this.Name = "Toten";
            this.Text = "Toten";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.webCamPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox webCamPictureBox;
        private System.Windows.Forms.Timer timerLogin;
        private System.Windows.Forms.Button buttonRegister;
        private System.Windows.Forms.Button buttonDispatch;
        private System.Windows.Forms.Timer timerCam;
        private System.Windows.Forms.Button buttonQueue;
        private System.Windows.Forms.Label labelOpenQueue;
        private System.Windows.Forms.Button buttonManualRead;
        private System.Windows.Forms.TextBox textBoxManualRead;
        private System.Windows.Forms.Button buttonAutoRead;
        private System.Windows.Forms.Label labelManualRead;
        private System.Windows.Forms.TextBox textBoxMessage;
    }
}